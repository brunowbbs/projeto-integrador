import firebase from 'firebase';

const firebaseConfig = {
  apiKey: 'AIzaSyDJTU9qQ9-gNn90mAQhZo9xxMqWj5ijqq8',
  authDomain: 'ecoapp-ed5fd.firebaseapp.com',
  databaseURL: 'https://ecoapp-ed5fd.firebaseio.com',
  projectId: 'ecoapp-ed5fd',
  storageBucket: 'ecoapp-ed5fd.appspot.com',
  messagingSenderId: '429826558016',
  appId: '1:429826558016:web:d3e13b519543361d9237a5',
  measurementId: 'G-X9XVE1NPVD',
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);

export default firebase;
