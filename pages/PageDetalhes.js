import React, {Component} from 'react';
import {
  View,
  Text,
  StatusBar,
  Image,
  Dimensions,
  StyleSheet,
  TouchableOpacity,
  ScrollView,
} from 'react-native';
import Icon from 'react-native-vector-icons/dist/FontAwesome';

const WIDTH = Dimensions.get('window').width;
const HEIGHT = Dimensions.get('window').height;

export default class pages extends Component {
  constructor(props) {
    super(props);

    this.title = this.props.navigation.getParam('titulo');
    this.anunciante = this.props.navigation.getParam('nomeAnunciante');
    this.quantidade = this.props.navigation.getParam('quantidade');
    this.telefone = this.props.navigation.getParam('telefone');
    this.descricao = this.props.navigation.getParam('descricao');
    this.imagem = this.props.navigation.getParam('img');
  }
  render() {
    return (
      <View>
        <ScrollView>
          <StatusBar backgroundColor="#009432" />

          {/*HEADER*/}
          <View style={styles.ctnHeader}>
            <TouchableOpacity
              style={{padding: 10}}
              onPress={() => this.props.navigation.goBack()}>
              <Icon name="angle-left" size={27} color="#FFF" />
            </TouchableOpacity>
            <Text style={styles.title}>Detalhes do produto</Text>
          </View>

          <Image
            style={{width: WIDTH, height: HEIGHT * 0.3}}
            source={{uri: this.imagem}}
          />
          <Text
            style={{
              fontSize: 30,
              marginTop: '-12%',
              marginLeft: '2%',
              color: 'white',
              padding: '1%',
              fontFamily: 'Lato',
              fontWeight: 'bold',
            }}>
            {this.title}
          </Text>
          <View
            style={{
              marginTop: '1%',
              padding: '2%',
              paddingLeft: '5%',
              paddingRight: '5%',
            }}>
            <View>
              <Text style={{fontFamily: 'Lato', fontSize: 16, marginTop: '3%'}}>
                Anunciante:
              </Text>
              <Text style={{fontSize: 18, marginTop: '1%'}}>
                {this.anunciante}
              </Text>
            </View>
            <View>
              <Text style={{fontFamily: 'Lato', fontSize: 16, marginTop: '3%'}}>
                Telefone:
              </Text>
              <Text style={{fontSize: 18, marginTop: '1%'}}>
                {this.telefone}
              </Text>
            </View>
            <View>
              <Text style={{fontFamily: 'Lato', fontSize: 16, marginTop: '3%'}}>
                Quantidade aproximada de itens:
              </Text>
              <Text style={{fontSize: 18, marginTop: '1%'}}>
                {this.quantidade} kg(s)
              </Text>
            </View>
            <View>
              <Text style={{fontFamily: 'Lato', fontSize: 16, marginTop: '3%'}}>
                Descrição:
              </Text>
              <Text style={{fontSize: 18, marginTop: '1%'}}>
                {this.descricao}
              </Text>
            </View>
            <TouchableOpacity onPress={undefined} style={styles.button}>
              <Text style={styles.buttonText}>LIGAR PARA ANUNCIANTE</Text>
            </TouchableOpacity>
          </View>
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  ctnHeader: {
    paddingHorizontal: 12,
    alignItems: 'center',
    justifyContent: 'space-between',
    flexDirection: 'row',
    height: HEIGHT * 0.075,
    backgroundColor: '#009432',
  },
  title: {
    flex: 1,
    textAlign: 'center',
    fontSize: 18,
    color: 'white',
  },
  button: {
    backgroundColor: '#009432',
    borderRadius: 25,
    justifyContent: 'center',
    alignItems: 'center',
    height: 50,
    borderColor: 'white',
    borderWidth: 1,
    marginTop: '15%',
  },
  buttonText: {
    fontWeight: 'bold',
    color: 'white',
  },
});
