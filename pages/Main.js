import React, {Component} from 'react';
import {
  View,
  Text,
  Image,
  StyleSheet,
  StatusBar,
  TouchableOpacity,
} from 'react-native';

export default class Main extends Component {
  static navigationOptions = {
    header: null,
  };

  render() {
    const {navigation} = this.props;

    return (
      <View style={styles.container}>
        <StatusBar backgroundColor="#009432" />

        <View style={styles.ctnImg}>
          <Image
            style={styles.img}
            resizeMode="contain"
            source={require('../img/logo.png')}
          />
        </View>

        <View style={styles.ctnTxtLogo}>
          <Text style={styles.txtLogo1}>Eco</Text>
          <Text style={styles.txtLogo2}>APP</Text>
        </View>

        <View style={styles.ctnFrase}>
          <Text style={styles.frase}>
            Ganhe dinheiro vendendo materiais {'\n'} recicláveis.
          </Text>
        </View>

        <TouchableOpacity
          onPress={() => navigation.navigate('Login')}
          style={styles.btnSignUp}>
          <Text style={styles.txtSignUp}>ENTRAR</Text>
        </TouchableOpacity>

        <Text style={styles.cadText}>Ainda não possui cadastro?</Text>

        <TouchableOpacity
          onPress={() => navigation.navigate('Cadastro')}
          style={styles.btnLogin}>
          <Text style={styles.txtLogin}>CADASTRAR</Text>
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#009432',
    justifyContent: 'center',
    flex: 1,
    padding: 30,
  },
  ctnImg: {
    alignItems: 'center',
  },
  ctnTxtLogo: {
    marginTop: 20,
    marginBottom: 10,
    justifyContent: 'center',
    flexDirection: 'row',
  },
  ctnFrase: {
    marginTop: 50,
  },
  frase: {
    fontSize: 15,
    textAlign: 'center',
    color: 'white',
  },
  txtLogo1: {
    fontSize: 16,
    color: 'white',
  },
  txtLogo2: {
    fontSize: 16,
    color: 'white',
    fontWeight: 'bold',
  },
  btnSignUp: {
    borderRadius: 25,
    justifyContent: 'center',
    alignItems: 'center',
    height: 50,
    marginTop: 50,
    borderColor: 'white',
    borderWidth: 1,
  },
  txtSignUp: {
    fontWeight: 'bold',
    color: 'white',
  },
  btnLogin: {
    backgroundColor: 'white',
    borderRadius: 25,
    justifyContent: 'center',
    alignItems: 'center',
    height: 50,
    marginTop: 10,
    borderColor: 'white',
    borderWidth: 1,
  },
  txtLogin: {
    fontWeight: 'bold',
    color: '#009432',
  },
  img: {
    width: 80,
    height: 80,
  },

  cadText: {
    color: 'white',
    marginTop: 25,
    textAlign: 'center',
    justifyContent: 'center',
    alignItems: 'center',
  },
});
