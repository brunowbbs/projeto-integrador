import React, {Component} from 'react';
import {
  View,
  Text,
  Picker,
  Image,
  StatusBar,
  StyleSheet,
  Dimensions,
  TouchableOpacity,
  ScrollView,
  TextInput,
  KeyboardAvoidingView,
} from 'react-native';
import Icon from 'react-native-vector-icons/dist/FontAwesome';
import * as firebase from 'firebase';
import * as Progress from 'react-native-progress';
import ImagePicker from 'react-native-image-picker';
import RNFetchBlob from 'react-native-fetch-blob';

window.XMLHttpRequest = RNFetchBlob.polyfill.XMLHttpRequest;
window.Blob = RNFetchBlob.polyfill.Blob;

const HEIGHT = Dimensions.get('window').height;

export default class pages extends Component {
  constructor() {
    super();

    this.uid;
    this.key;
    this.url;

    firebase.auth().onAuthStateChanged(user => {
      if (user) {
        this.uid = user.uid;

        this.key = firebase
          .database()
          .ref('anuncios')
          .push().key;
      }
    });

    this.state = {
      loading: false,
      pct: 0,
      imagem: null,
      categoria: 0,
      loading: false,
      nomeAnunciante: '',
      telefone: '',
      titulo: '',
      quantidade: 0,
      descricao: '',
      categorias: [
        {nome: 'Selecione', codigo: 0},
        {nome: 'Plástico', codigo: 1},
        {nome: 'Papel', codigo: 2},
        {nome: 'Metal', codigo: 3},
        {nome: 'Outros', codigo: 4},
      ],
    };

    this.pegarFoto = this.pegarFoto.bind(this);
  }

  cadAnuncio = () => {
    if (this.state.pct !== 100) {
      alert('Antes de cadastrar, carregue uma foto para o anúncio');
    } else {
      firebase
        .database()
        .ref('anuncios')
        .child(this.key)
        .set({
          idUsuario: this.uid,
          idAnuncio: this.key,
          nomeAnunciante: this.state.nomeAnunciante,
          titulo: this.state.titulo,
          telefone: this.state.telefone,
          categoria: this.state.categoria,
          quantidade: this.state.quantidade,
          descricao: this.state.descricao,
          url: this.url,
          status: 0,
        });

      alert('Cadastrado');
      this.props.navigation.goBack();
    }
  };

  pegarFoto() {
    let options = {
      title: 'Selecione uma foto',
    };

    ImagePicker.showImagePicker(options, r => {
      if (r.uri) {
        let foto = {uri: r.uri};
        this.setState({imagem: foto});

        //transformando imagem em blob

        let uri = r.uri.replace('file://', '');
        let imagem = firebase
          .storage()
          .ref()
          .child('imagens')
          .child(`${this.key}.jpg`);
        let mime = 'imagem/jpeg';

        RNFetchBlob.fs
          .readFile(uri, 'base64')
          .then(data => {
            return RNFetchBlob.polyfill.Blob.build(data, {
              type: mime + ';BASE64',
            });
          })
          .then(blob => {
            imagem.put(blob, {contentType: mime}).on(
              'state_changed',
              snapshot => {
                let pct =
                  (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
                this.setState({pct});
              },
              error => {
                alert(error.code);
              },
              () => {
                this.url = imagem.getDownloadURL();
              },
            );
          });
      }
    });
  }

  render() {
    let categoriasPicker = this.state.categorias.map((item, index) => {
      return <Picker.Item key={index} value={index} label={item.nome} />;
    });

    return (
      <ScrollView>
        <StatusBar backgroundColor="#009432" />
        {/*HEADER*/}
        <View style={styles.ctnHeader}>
          <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
            <Icon name="angle-left" size={27} color="#FFF" />
          </TouchableOpacity>
          <Text style={styles.title}>Novo anúncio</Text>
        </View>

        {/*IMAGEM*/}
        <View style={styles.ctnImage}>
          <Image
            source={
              this.state.imagem === null
                ? require('../img/iconImg.png')
                : this.state.imagem
            }
            style={{
              width: '100%',
              flex: 1,
              justifyContent: 'center',
              alignItems: 'center',
              height: '100%',
            }}
            resizeMode="contain"
          />
        </View>
        {/*FLOAT*/}
        <TouchableOpacity
          onPress={this.pegarFoto}
          style={{
            width: 60,
            justifyContent: 'center',
            alignItems: 'center',
            height: 60,
            borderRadius: 40,
            position: 'absolute',
            right: '5%',
            top: HEIGHT * 0.215,
            backgroundColor: '#009432',
            elevation: 7,
          }}>
          <Icon name="camera" size={20} color="white" />
        </TouchableOpacity>

        {/*FORMULÁRIO*/}

        <View style={styles.ctnForm}>
          <KeyboardAvoidingView>
            <View style={{marginBottom: '2%'}}>
              <View style={{marginBottom: '2%'}}>
                <Text style={styles.fields}>Nome do anunciante</Text>
                <TextInput
                  style={styles.input}
                  onChangeText={nomeAnunciante =>
                    this.setState({nomeAnunciante})
                  }
                />
              </View>
              <View style={{marginBottom: '2%'}}>
                <Text style={styles.fields}>Título do anúncio</Text>
                <TextInput
                  style={styles.input}
                  onChangeText={titulo => this.setState({titulo})}
                />
              </View>
              <View style={{marginBottom: '2%'}}>
                <Text style={styles.fields}>Quantidade aproximada (kg)</Text>
                <TextInput
                  style={styles.input}
                  keyboardType="numeric"
                  onChangeText={quantidade => this.setState({quantidade})}
                />
              </View>

              <Text style={styles.fields}>Telefone para contato</Text>
              <TextInput
                style={styles.input}
                keyboardType="phone-pad"
                onChangeText={telefone => this.setState({telefone})}
              />
            </View>
            <View style={{marginBottom: '2%'}}>
              <Text style={styles.fields}>Selecione uma categoria</Text>
              <Picker
                selectedValue={this.state.categoria}
                onValueChange={itemValue =>
                  this.setState({categoria: itemValue})
                }>
                {categoriasPicker}
              </Picker>
            </View>
            <View style={{marginBottom: '2%'}}>
              <Text style={styles.fields}>
                Escreva um descrição do seu produto
              </Text>
              <TextInput
                style={styles.input}
                multiline={true}
                maxLength={150}
                onChangeText={descricao => this.setState({descricao})}
              />
            </View>
            <View style={{marginBottom: '2%'}}>
              {this.state.loading === false ? (
                <TouchableOpacity
                  onPress={this.cadAnuncio}
                  style={styles.button}>
                  <Text style={styles.buttonText}>CADASTRAR</Text>
                </TouchableOpacity>
              ) : (
                <View style={styles.button}>
                  <Progress.Circle
                    size={20}
                    indeterminate={true}
                    color="white"
                  />
                </View>
              )}
            </View>
          </KeyboardAvoidingView>
        </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  ctnHeader: {
    paddingHorizontal: 12,
    alignItems: 'center',
    justifyContent: 'space-between',
    flexDirection: 'row',
    height: HEIGHT * 0.075,
    backgroundColor: '#009432',
  },
  fields: {
    fontSize: 16,
    fontFamily: 'RobotoLight',
  },
  title: {
    flex: 1,
    textAlign: 'center',
    fontSize: 18,
    color: 'white',
  },
  ctnImage: {
    justifyContent: 'center',
    alignItems: 'center',
    height: HEIGHT * 0.18,
    backgroundColor: '#f5f5f5',
  },
  ctnForm: {
    padding: '3%',
    marginTop: '10%',
  },
  input: {
    paddingHorizontal: '3%',
    height: 45,
    fontSize: 17,
    borderWidth: 0.4,
    borderRadius: 5,
  },
  button: {
    backgroundColor: '#009432',
    borderRadius: 25,
    justifyContent: 'center',
    alignItems: 'center',
    height: 50,
    marginTop: 12,
    borderColor: 'white',
    borderWidth: 1,
  },
  buttonText: {
    fontWeight: 'bold',
    color: 'white',
  },
});
