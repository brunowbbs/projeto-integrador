console.disableYellowBox = true;

import React, {Component} from 'react';
import {
  KeyboardAvoidingView,
  View,
  Text,
  StyleSheet,
  StatusBar,
  TextInput,
  TouchableOpacity,
  Image,
  Alert,
  ScrollView,
} from 'react-native';
import {StackActions, NavigationActions} from 'react-navigation';
import * as Progress from 'react-native-progress';
import firebase from '../FirebaseConfig';

export default class Cadastro extends Component {
  static navigationOptions = {
    headerTintColor: 'white',
    title: 'Cadastrar',
    headerStyle: {
      backgroundColor: '#009432',
    },
  };

  constructor() {
    super();

    firebase.auth().signOut();

    this.state = {
      loading: false,
      nome: '',
      email: '',
      senha: '',
      senha2: '',
    };

    firebase.auth().onAuthStateChanged(user => {
      if (user) {
        firebase
          .database()
          .ref('users')
          .child(user.uid)
          .set({nome: 'Wesley'})
          .then(() => {
            this.props.navigation.dispatch(
              StackActions.reset({
                index: 0,
                actions: [NavigationActions.navigate({routeName: 'Home'})],
              }),
            );
          })
          .catch(error => alert(error.code));
      }
    });
  }

  cadastrar = () => {
    this.setState({loading: true});
    if (
      this.state.nome === '' ||
      this.state.email === '' ||
      this.state.senha === '' ||
      this.state.senha2 === '' ||
      this.state.senha !== this.state.senha2
    ) {
      Alert.alert(
        'Erro ao Cadastrar',
        'O cadastro não pode ser finalizado! Verifique os dados inseridos e tente novamente.',
        [{text: 'OK'}],
        {cancelable: false},
      );
      this.setState({loading: false});
    } else {
      firebase
        .auth()
        .createUserWithEmailAndPassword(this.state.email, this.state.senha)
        .catch(error => {
          this.setState({loading: false});
          Alert.alert('Erro ao cadastrar', error.message);
        });
    }
  };

  render() {
    const {navigation} = this.props;
    return (
      <View style={styles.container}>
        <StatusBar backgroundColor="#009432" />
        <ScrollView>
          <View style={styles.ctnImg}>
            <Image
              style={styles.img}
              resizeMode="contain"
              source={require('../img/logoverde.png')}
            />
          </View>
          <View style={styles.ctnTxtLogo}>
            <Text style={styles.txtLogo1}>Eco</Text>
            <Text style={styles.txtLogo2}>APP</Text>
          </View>

          <KeyboardAvoidingView>
            <Text style={styles.label}>NOME</Text>
            <TextInput
              onChangeText={nome => this.setState({nome})}
              style={styles.input}
              placeholder="Digite seu nome"
            />

            <Text style={styles.label}>EMAIL</Text>
            <TextInput
              onChangeText={email => this.setState({email})}
              style={styles.input}
              placeholder="email@ecoapp.com.br"
            />

            <Text style={styles.label}>SENHA</Text>
            <TextInput
              onChangeText={senha => this.setState({senha})}
              style={styles.input}
              placeholder="*********"
              secureTextEntry={true}
            />
            <Text style={styles.label}>SENHA</Text>
            <TextInput
              onChangeText={senha2 => this.setState({senha2})}
              style={styles.input}
              placeholder="*********"
              secureTextEntry={true}
            />

            <TouchableOpacity onPress={() => navigation.goBack()}>
              <Text style={styles.recSenha}>Já possui cadastro?</Text>
            </TouchableOpacity>

            {this.state.loading === false ? (
              <TouchableOpacity style={styles.button} onPress={this.cadastrar}>
                <Text style={styles.buttonText}>CADASTRAR</Text>
              </TouchableOpacity>
            ) : (
              <View
                style={styles.button}
                onPress={() => this.setState({loading: true})}>
                <Progress.Circle size={20} indeterminate={true} color="white" />
              </View>
            )}
          </KeyboardAvoidingView>
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'white',
    justifyContent: 'center',
    flex: 1,
    padding: 30,
  },
  ctnImg: {
    alignItems: 'center',
  },
  img: {
    width: 80,
    height: 80,
  },
  input: {
    borderBottomWidth: 1,
    borderBottomColor: '#009432',
    borderRadius: 5,
    height: 44,
    marginBottom: 20,
  },
  button: {
    backgroundColor: '#009432',
    borderRadius: 25,
    justifyContent: 'center',
    alignItems: 'center',
    height: 50,
    marginTop: 12,
    borderColor: 'white',
    borderWidth: 1,
  },
  buttonText: {
    fontWeight: 'bold',
    color: 'white',
  },
  label: {
    color: '#009432',
    marginTop: 5,
    fontWeight: 'bold',
  },
  ctnTxtLogo: {
    marginBottom: 60,
    marginTop: 15,
    justifyContent: 'center',
    flexDirection: 'row',
  },
  txtLogo1: {
    fontSize: 16,
    color: '#009432',
  },
  txtLogo2: {
    fontSize: 16,
    color: '#009432',
    fontWeight: 'bold',
  },
  recSenha: {
    marginTop: 20,
    textAlign: 'center',
    color: '#009432',
  },
});
