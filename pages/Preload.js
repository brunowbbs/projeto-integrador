import React, {Component} from 'react';
import {View, Text, StyleSheet, StatusBar, Image} from 'react-native';
import {StackActions, NavigationActions} from 'react-navigation';

import * as Progress from 'react-native-progress';

export default class Login extends Component {
  static navigationOptions = {
    header: null,
  };

  constructor() {
    super();
  }

  componentDidMount() {
    setTimeout(() => {
      this.props.navigation.dispatch(
        StackActions.reset({
          index: 0,
          actions: [NavigationActions.navigate({routeName: 'Main'})],
        }),
      );
    }, 3000);
  }

  render() {
    return (
      <View style={styles.container}>
        <StatusBar backgroundColor="#009432" />

        <View style={styles.ctnImg}>
          <Image
            style={styles.img}
            resizeMode="contain"
            source={require('../img/logo.png')}
          />
        </View>
        <View style={styles.ctnTxtLogo}>
          <Text style={styles.txtLogo1}>Eco</Text>
          <Text style={styles.txtLogo2}>APP</Text>
        </View>
        <View style={styles.ctnLoading}>
          <Progress.Circle size={20} indeterminate={true} color="white" />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#009432',
    justifyContent: 'center',
    flex: 1,
    padding: 30,
  },
  ctnLoading: {
    alignItems: 'center',
  },
  ctnImg: {
    alignItems: 'center',
  },
  img: {
    width: 80,
    height: 80,
  },
  ctnTxtLogo: {
    marginTop: 20,
    marginBottom: 20,
    justifyContent: 'center',
    flexDirection: 'row',
  },
  txtLogo1: {
    fontSize: 19,
    color: 'white',
  },
  txtLogo2: {
    fontSize: 19,
    color: 'white',
    fontWeight: 'bold',
  },
});
