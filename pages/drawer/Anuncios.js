import React, {Component, useRef} from 'react';
import {
  View,
  Text,
  StatusBar,
  Dimensions,
  StyleSheet,
  TouchableOpacity,
  TouchableHighlight,
  FlatList,
  Image,
} from 'react-native';
import Icon from 'react-native-vector-icons/dist/FontAwesome5';
import * as firebase from 'firebase';

const HEIGHT = Dimensions.get('window').height;

export default class drawer extends Component {
  navBtn(text, nome, bg, func) {
    return (
      <TouchableOpacity onPress={func}>
        <View style={{alignItems: 'center'}}>
          <View style={[styles.btnView, styles.btnMenu, {backgroundColor: bg}]}>
            <Icon name={text} size={24} color="#fff" />
          </View>
          <Text style={{marginTop: 3, fontSize: 12, fontFamily: 'RobotoLight'}}>
            {nome}
          </Text>
        </View>
      </TouchableOpacity>
    );
  }

  constructor(props) {
    super(props);

    this.state = {
      dados: [],
    };

    //Listener de Anuncios
    firebase
      .database()
      .ref(`anuncios`)
      .on('value', snapshot => {
        let state = this.state;
        state.dados = [];
        snapshot.forEach(childItem => {
          state.dados.push({
            key: childItem.key,

            titulo: childItem.val().titulo,
            nomeAnunciante: childItem.val().nomeAnunciante,
            quantidade: childItem.val().quantidade,
            img: childItem.val().url.i,
            telefone: childItem.val().telefone,
            descricao: childItem.val().descricao,
          });
        });
        this.setState(state);
      });
  }

  all = () => {
    firebase
      .database()
      .ref(`anuncios`)
      .on('value', snapshot => {
        let state = this.state;
        state.dados = [];
        snapshot.forEach(childItem => {
          state.dados.push({
            key: childItem.key,

            titulo: childItem.val().titulo,
            nomeAnunciante: childItem.val().nomeAnunciante,
            quantidade: childItem.val().quantidade,
            img: childItem.val().url.i,
            telefone: childItem.val().telefone,
            descricao: childItem.val().descricao,
          });
        });
        this.setState(state);
      });
  };

  plastico = () => {
    firebase
      .database()
      .ref('anuncios')
      .once('value', snapshot => {
        let state = this.state;
        state.dados = [];
        snapshot.forEach(childItem => {
          if (childItem.val().categoria == 1) {
            state.dados.push({
              key: childItem.key,

              titulo: childItem.val().titulo,
              nomeAnunciante: childItem.val().nomeAnunciante,
              quantidade: childItem.val().quantidade,
              img: childItem.val().url.i,
              telefone: childItem.val().telefone,
              descricao: childItem.val().descricao,
            });
          }
        });
        this.setState(state);
      });
  };

  papel = () => {
    firebase
      .database()
      .ref('anuncios')
      .once('value', snapshot => {
        let state = this.state;
        state.dados = [];
        snapshot.forEach(childItem => {
          if (childItem.val().categoria == 2) {
            state.dados.push({
              key: childItem.key,

              titulo: childItem.val().titulo,
              nomeAnunciante: childItem.val().nomeAnunciante,
              quantidade: childItem.val().quantidade,
              img: childItem.val().url.i,
              telefone: childItem.val().telefone,
              descricao: childItem.val().descricao,
            });
          }
        });
        this.setState(state);
      });
  };

  metal = () => {
    firebase
      .database()
      .ref('anuncios')
      .once('value', snapshot => {
        let state = this.state;
        state.dados = [];
        snapshot.forEach(childItem => {
          if (childItem.val().categoria == 3) {
            state.dados.push({
              key: childItem.key,

              titulo: childItem.val().titulo,
              nomeAnunciante: childItem.val().nomeAnunciante,
              quantidade: childItem.val().quantidade,
              img: childItem.val().url.i,
              telefone: childItem.val().telefone,
              descricao: childItem.val().descricao,
            });
          }
        });
        this.setState(state);
      });
  };

  outros = () => {
    firebase
      .database()
      .ref('anuncios')
      .once('value', snapshot => {
        let state = this.state;
        state.dados = [];
        snapshot.forEach(childItem => {
          if (childItem.val().categoria == 4) {
            state.dados.push({
              key: childItem.key,

              titulo: childItem.val().titulo,
              nomeAnunciante: childItem.val().nomeAnunciante,
              quantidade: childItem.val().quantidade,
              img: childItem.val().url.i,
              telefone: childItem.val().telefone,
              descricao: childItem.val().descricao,
            });
          }
        });
        this.setState(state);
      });
  };

  render() {
    return (
      <View style={{flex: 1}}>
        <StatusBar backgroundColor="#009432" />

        {/*HEADER*/}
        <View style={styles.ctnHeader}>
          <TouchableOpacity
            onPress={() => this.props.navigation.toggleDrawer()}>
            <Icon name="bars" size={27} color="#FFF" />
          </TouchableOpacity>
          <Text style={styles.title}>EcoApp</Text>
          <Icon name="search" size={23} color="#FFF" />
        </View>

        <Text style={{textAlign: 'center', fontFamily: 'RobotoLight'}}>
          Selecione uma categoria
        </Text>
        {/*MENU*/}
        <View style={styles.ctnButtons}>
          {this.navBtn('diaspora', 'Todas', '#2f3640', this.all)}
          {this.navBtn('wine-bottle', 'Plástico', '#e74c3c', this.plastico)}
          {this.navBtn('box', 'Papel', '#3498db', this.papel)}
          {this.navBtn('calendar-week', ' Metal', '#f1c40f', this.metal)}
          {this.navBtn('funnel-dollar', ' Outros', '#27ae60', this.outros)}
        </View>

        {/*FLATLIST */}
        <FlatList
          style={{
            marginTop: 5,
            backgroundColor: '#DDD',
            borderTopWidth: 0.2,
            flex: 1,
          }}
          data={this.state.dados}
          renderItem={({item}) => (
            <TouchableOpacity
              onPress={() =>
                this.props.navigation.navigate('PageDetalhes', {
                  img: item.img,
                  titulo: item.titulo,
                  nomeAnunciante: item.nomeAnunciante,
                  telefone: item.telefone,
                  quantidade: item.quantidade,
                  descricao: item.descricao,
                })
              }>
              <View style={styles.container}>
                <Image
                  style={{width: 100, height: 80}}
                  source={{uri: item.img}}
                />
                <View style={styles.containerText}>
                  <Text style={styles.titulo}>{item.titulo}</Text>
                  <Text style={styles.preco}>{item.quantidade}kg em média</Text>
                  <Text style={styles.localizacao}>
                    Anunciante: {item.nomeAnunciante}
                  </Text>
                </View>
              </View>
            </TouchableOpacity>
          )}
        />
        <TouchableOpacity
          onPress={() => this.props.navigation.navigate('CadastroAnuncio')}
          style={{
            width: 60,
            justifyContent: 'center',
            alignItems: 'center',
            height: 60,
            borderRadius: 40,
            position: 'absolute',
            bottom: 12,
            right: 12,
            backgroundColor: '#009432',
            elevation: 7,
          }}>
          <Icon name="plus" size={15} color="white" />
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  ctnHeader: {
    paddingHorizontal: 12,
    alignItems: 'center',
    justifyContent: 'space-between',
    flexDirection: 'row',
    height: HEIGHT * 0.075,
    backgroundColor: '#009432',
  },
  title: {
    fontSize: 18,
    color: 'white',
  },

  ctnButtons: {
    backgroundColor: 'white',
    margin: 10,
    padding: 7,
    justifyContent: 'space-between',
    flexDirection: 'row',
    alignItems: 'center',
    height: HEIGHT * 0.1,
  },

  btnMenu: {
    justifyContent: 'center',
    width: HEIGHT * 0.065,
    height: HEIGHT * 0.065,
    borderRadius: (HEIGHT * 0.1) / 2,
  },
  btnView: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  container: {
    marginVertical: 2,
    paddingHorizontal: 10,
    paddingVertical: 5,
    flexDirection: 'row',
    backgroundColor: '#FFF',
  },
  containerText: {
    marginLeft: 15,
    flexDirection: 'column',
  },
  titulo: {
    color: '#009432',
    fontFamily: 'Roboto',
    marginBottom: 5,
    fontSize: 15,
  },
  preco: {
    fontFamily: 'RobotoLight',
    fontSize: 16,
  },
  localizacao: {
    fontFamily: 'RobotoLight',

    marginTop: 10,
    fontSize: 12,
  },
});
