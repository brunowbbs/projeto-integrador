import React from 'react';
import {createDrawerNavigator} from 'react-navigation-drawer';
import {Platform, Dimensions} from 'react-native';
import MenuDrawer from '../components/Drawer';

import Anuncios from '../pages/drawer/Anuncios';
import Favoritos from '../pages/drawer/Favoritos';
import MeusAnuncios from './drawer/MeusAnuncios';

const WIDTH = Dimensions.get('window').width;

const DrawerConfig = {
  drawerWidth: WIDTH * 0.63,
  contentComponent: ({navigation}) => {
    return <MenuDrawer />;
  },
};

export default createDrawerNavigator(
  {
    Anuncios: {
      screen: Anuncios,
    },
    Favoritos,
    MeusAnuncios,
  },
  DrawerConfig,
);
