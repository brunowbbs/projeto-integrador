import React, {Component} from 'react';
import {
  View,
  Text,
  StyleSheet,
  StatusBar,
  TextInput,
  TouchableOpacity,
  Image,
  Alert,
} from 'react-native';

import {StackActions, NavigationActions} from 'react-navigation';

import * as Progress from 'react-native-progress';

import firebase from '../FirebaseConfig';

export default class Login extends Component {
  static navigationOptions = {
    headerTintColor: 'white',
    title: 'Entrar',
    headerStyle: {
      backgroundColor: '#009432',
    },
  };

  constructor() {
    super();
    this.state = {
      email: '',
      senha: '',

      loading: false,
    };
  }

  logar = () => {
    if (this.state.email === '' || this.state.senha === '') {
      Alert.alert('Erro ao entrar', 'Há campo(s) vazio(s).');
    } else {
      this.setState({loading: true});
      firebase
        .auth()
        .signInWithEmailAndPassword(this.state.email, this.state.senha)
        .then(() => {
          //dispatch envia uma ação ao roteador
          this.props.navigation.dispatch(
            //StackActions é um objeto que contém métodos para gerar ações específicas para navegadores baseados em pilha
            StackActions.reset({
              // novo índice ativo
              index: 0,
              //Matriz de ações de navegação que substituirão o estado de navegação
              actions: [NavigationActions.navigate({routeName: 'Home'})],
            }),
          );
        })
        .catch(error => {
          Alert.alert('Erro ao entrar', error.message);
          this.setState({loading: false});
        });
    }
  };

  render() {
    const {navigation} = this.props;

    return (
      <View style={styles.container}>
        <StatusBar backgroundColor="#009432" />

        <View style={styles.ctnImg}>
          <Image
            style={styles.img}
            resizeMode="contain"
            source={require('../img/logoverde.png')}
          />
        </View>
        <View style={styles.ctnTxtLogo}>
          <Text style={styles.txtLogo1}>Eco</Text>
          <Text style={styles.txtLogo2}>APP</Text>
        </View>

        <Text style={styles.label}>EMAIL</Text>
        <TextInput
          style={styles.input}
          onChangeText={email => this.setState({email})}
          placeholder="email@ecoapp.com.br"
        />

        <Text style={styles.label}>SENHA</Text>
        <TextInput
          onChangeText={senha => this.setState({senha})}
          style={styles.input}
          placeholder="*********"
          secureTextEntry={true}
        />

        <TouchableOpacity onPress={() => navigation.goBack()}>
          <Text style={styles.recSenha}>Esqueceu a senha?</Text>
        </TouchableOpacity>

        {this.state.loading === false ? (
          <TouchableOpacity onPress={this.logar} style={styles.button}>
            <Text style={styles.buttonText}>ENTRAR</Text>
          </TouchableOpacity>
        ) : (
          <View style={styles.button}>
            <Progress.Circle size={20} indeterminate={true} color="white" />
          </View>
        )}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'white',
    justifyContent: 'center',
    flex: 1,
    padding: 30,
  },
  ctnImg: {
    alignItems: 'center',
  },
  img: {
    width: 80,
    height: 80,
  },
  input: {
    borderBottomWidth: 1,
    borderBottomColor: '#009432',
    borderRadius: 5,
    height: 44,
    marginBottom: 20,
  },
  button: {
    backgroundColor: '#009432',
    borderRadius: 25,
    justifyContent: 'center',
    alignItems: 'center',
    height: 50,
    marginTop: 12,
    borderColor: 'white',
    borderWidth: 1,
  },
  buttonText: {
    fontWeight: 'bold',
    color: 'white',
  },
  label: {
    fontWeight: 'bold',
    color: '#009432',
    marginTop: 5,
  },
  ctnTxtLogo: {
    marginBottom: 60,
    marginTop: 15,
    justifyContent: 'center',
    flexDirection: 'row',
  },
  txtLogo1: {
    fontSize: 16,
    color: '#009432',
  },
  txtLogo2: {
    fontSize: 16,
    color: '#009432',
    fontWeight: 'bold',
  },
  recSenha: {
    marginTop: 20,
    textAlign: 'center',
    color: '#009432',
  },
});
