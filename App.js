import {createAppContainer} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';

import Preload from './pages/Preload';
import Main from './pages/Main';
import Login from './pages/Login';
import Cadastro from './pages/Cadastro';
import Home from './pages/Home';
import CadastroAnuncio from './pages/CadastroAnuncio';
import PageDetalhes from './pages/PageDetalhes';

export default createAppContainer(
  createStackNavigator(
    {
      Home: {
        screen: Home,
      },
      Preload: {
        screen: Preload,
      },
      Home: {
        screen: Home,
      },
      CadastroAnuncio: {
        screen: CadastroAnuncio,
      },

      Main: {
        screen: Main,
      },
      Login: {
        screen: Login,
      },
      Cadastro: {
        screen: Cadastro,
      },
      PageDetalhes: {
        screen: PageDetalhes,
      },
    },
    {
      defaultNavigationOptions: {
        header: null,
      },
    },
  ),
);
