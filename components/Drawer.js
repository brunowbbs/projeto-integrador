import React from 'react';
import {
  Dimensions,
  View,
  Text,
  Image,
  StyleSheet,
  TouchableHighlight,
} from 'react-native';
import Icon from 'react-native-vector-icons/dist/FontAwesome';

export default class Drawer extends React.Component {
  constructor(props) {
    super(props);
  }

  navLink(nav, text) {
    return (
      <TouchableHighlight
        onPress={() => this.props.navigation.navigate(nav)}
        style={{
          height: 50,
          borderBottomColor: '#CCC',
          borderBottomWidth: 0.3,
        }}>
        <Text style={styles.link}>{text}</Text>
      </TouchableHighlight>
    );
  }

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.topLinks}>
          <View style={styles.profile}>
            <View style={styles.imageView}>
              <Icon name="snapchat-ghost" size={30} color="#FFF" />
              <Text
                style={{
                  color: 'white',
                  marginTop: 20,
                  fontSize: 18,
                  fontFamily: 'RobotoLight',
                }}>
                Usuário
              </Text>
            </View>
          </View>
        </View>
        <View style={styles.bottomLinks}>
          {this.navLink('MeusAnuncios', 'Meus anúncios')}
          {this.navLink('Alunos', 'Favoritos')}
          {this.navLink('Financeiro', 'Meus Dados')}
          {this.navLink('Sair', 'Sair')}
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  concoiner: {
    flex: 1,
  },
  topLinks: {
    height: 200,
    backgroundColor: '#009432',
  },
  bottomLinks: {
    flex: 1,
    backgroundColor: 'white',
    paddingTop: 10,
    paddingBottom: 450,
  },
  link: {
    fontFamily: 'RobotoLight',
    flex: 1,
    fontSize: 16,
    padding: 6,
    paddingLeft: 14,
    margin: 5,
    textAlign: 'left',
    color: 'black',
  },
  profile: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    paddingTop: 25,
    borderBottomWidth: 1,
    borderBottomColor: '#777',
  },
  imageView: {
    flex: 3,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },
  img: {
    height: 100,
    width: 100,
    borderRadius: 50,
  },
});
